Source: python-dbutils
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Dale Richards <dale@dalerichards.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-setuptools,
 python3-all,
 python3-pygresql,
 python3-pytest,
 pybuild-plugin-pyproject,
Testsuite: autopkgtest-pkg-pybuild
Standards-Version: 4.6.2
Homepage: https://webwareforpython.github.io/DBUtils/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-dbutils
Vcs-Git: https://salsa.debian.org/python-team/packages/python-dbutils.git

Package: python-dbutils-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: documentation for the dbtools Python library
 DBUtils is a suite of tools providing solid, persistent and pooled
 connections to a database that can be used in all kinds of multi-threaded
 environments.
 .
 This package provides documentation for python3-dbtools

Package: python3-dbutils
Architecture: all
Multi-Arch: foreign
Depends:
 python3-pygresql,
 ${python3:Depends},
 ${misc:Depends},
Description: tools for providing connections to a database (Python 3)
 DBUtils is a suite of tools providing solid, persistent and pooled
 connections to a database that can be used in all kinds of multi-threaded
 environments.
 .
 The suite supports DB-API 2 compliant database interfaces and the classic
 PyGreSQL interface.
 .
 This package installs the library for Python 3.
